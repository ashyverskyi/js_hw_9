/* 
Теоретичні питання
1. Опишіть своїми словами що таке Document Object Model (DOM)
   DOM - це інтерфейс програмування, який представляє структуру HTML-документа у вигляді дерева об'єктів, що дозволяє JavaScript змінювати вміст, стиль та структуру веб-сторінок.

2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?
   Властивість innerHTML містить HTML-розмітку внутрішнього вмісту елемента і дозволяє отримувати або встановлювати цю розмітку.
   Властивість innerText містить тільки текстовий вміст елемента, виключаючи HTML-розмітку, і також дозволяє отримувати або встановлювати цей текстовий вміст.

3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
   document.getElementById(), document.getElementsByClassName(), document.getElementsByTagName(), document.querySelector(), document.querySelectorAll(). Я, наче професійний стоматолог, з десятирічним досвідом роботи, РЕКОМЕНДУЮ користуватись способом "document.querySelector()" три рази на день після їжі.

4. Яка різниця між nodeList та HTMLCollection?
   Обидва типи можна ітерувати і отримувати доступ до елементів за індексом, але їхнє походження та динамічність можуть відрізнятися.


Практичні завдання
 1. Знайдіть всі елементи з класом "feature", запишіть в змінну, вивести в консоль.
 Використайте 2 способи для пошуку елементів. 
 Задайте кожному елементу з класом "feature" вирівнювання тексту по - центру(text-align: center).
 
 2. Змініть текст усіх елементів h2 на "Awesome feature".

 3. Знайдіть всі елементи з класом "feature-title" та додайте в кінець тексту елементу знак оклику "!".
 */

// TASK 1

const elementsSolution1 = document.querySelectorAll('.feature');
const elementsSolution2 = document.getElementsByClassName('feature');

elementsSolution1.forEach(element => {
    element.style.textAlign = 'center';
})

console.log(elementsSolution1);
console.log(elementsSolution2);

// TASK 2

const task2 = document.querySelectorAll('h2');

task2.forEach(magicFunc => {
    // magicFunc.textContent = 'Awesome feature';
    magicFunc.innerHTML = 'Awesome feature';
});

// TASK 3

const task3 = document.querySelectorAll('.feature-title');

task3.forEach(happyFunc => {
    happyFunc.textContent += '!';
});





 